function isDataSheet(file){
  var spreadsheet = SpreadsheetApp.open(file);
  var sheetMap = new Map();
  var sheets = spreadsheet.getSheets();
  for(var i = 0; i < sheets.length; i ++){
    sheetMap.set(sheets[i].getName(),sheets[i]);
  }
  var eventSheet = sheetMap.get('event');
  if(eventSheet){
    return true;
  }
  return false;
}

function openEventSheet(file, parentName){
  var spreadsheet = SpreadsheetApp.open(file);
  var sheetMap = new Map();
  var sheets = spreadsheet.getSheets();
  for(var i = 0; i < sheets.length; i ++){
    sheetMap.set(sheets[i].getName(),sheets[i]);
  }
  var eventSheet = sheetMap.get('event');
  var userSheet = sheetMap.get('user');

  if(eventSheet){
    Logger.log("has event sheet.");
    // create calendar
    var calendar = createCalendar(parentName + "/" + file.getName());

    // create events
    var eventData = eventSheet.getRange("A2:F").getValues();
    var eventDisplayData = eventSheet.getRange("A2:F").getDisplayValues();
    var eventList = [];
    for(var i = 0; i < eventData.length; i ++){
          if(eventData[i][0]){
            createEvent(calendar,
                eventData[i][0],
                eventData[i][1],
                eventData[i][2],
                eventData[i][3],
                eventData[i][4],
                eventData[i][5]);
            eventList.push(eventDisplayData[i][0] + "\n" + eventDisplayData[i][1] + " " + eventDisplayData[i][2] + "-" + eventDisplayData[i][3]);
          }else{
            break;
          }
    }

    // share calendar
    if(!userSheet){
      userSheet = spreadsheet.insertSheet();
      userSheet.setName("user");
    }

    // add event title
    userSheet.getRange("A1:A1").setValue("Name");
    userSheet.getRange("B1:B1").setValue("User Type");

    var eventTitleRange = userSheet.getRange(1, 4, 1, eventList.length);
    eventTitleRange.setValues([eventList]);

    // set sum
    var sumValues = [["Sum"]];
    for(var i=2; i <= 200; i ++){
      sumValues.push(["=sum(D"+i +":AZ"+i+")"]);
    }
    var sumTitleRange = userSheet.getRange(1, 3, 200, 1);
    sumTitleRange.setValues(sumValues);

    // set alignment
    userSheet.getRange("A1:CC1").setHorizontalAlignment("center").setVerticalAlignment("middle");

    // add data validation
    setValidation(userSheet, 'A2:A', getUserListRule());
    setValidation(userSheet, 'B2:B', getUserRoleListRule());

    // share calendar to users
    var data = userSheet.getRange("A2:B").getValues();
    for(var i = 0; i < data.length; i ++){
      var user = userList.get(data[i][0]);
      var userType = data[i][1];
      var email = user[6];
      var parentEmail = user[11];
      shareCalendar(calendar, email, getCalendarRole(userType));
      shareCalendar(calendar, parentEmail, getCalendarRole(userType));
    }

    // set layout
    userSheet.autoResizeRows(1,1);
    userSheet.autoResizeColumns(4,eventList.length);
    userSheet.setFrozenColumns(3);
    userSheet.setFrozenRows(1);
  }

  // create classroom
  // var course = createCourse(parentName + "/" + file.getName());
  // addTeacher(course, userSheet.getRange('B2').getValue());
  
}

function openSheet(file) {
  var spreadsheet = SpreadsheetApp.open(file);
  var sheet = spreadsheet.getSheets()[0];
  Logger.log(sheet.getName());
}

function getSheetDataMap(id, sheetName, dataRange, keyColumn) {
  var spreadsheet = SpreadsheetApp.openById(id);

  // get all sheets
  var sheetMap = new Map();
  var sheets = spreadsheet.getSheets();
  for(var i = 0; i < sheets.length; i ++){
    sheetMap.set(sheets[i].getName(),sheets[i]);
  }
  var sheet = sheetMap.get(sheetName);

  // get all data
  var data = sheet.getRange(dataRange).getValues();
  var dataMap = new Map();
  for(var i = 0; i < data.length; i ++){
    dataMap.set(data[i][keyColumn], data[i]);
  }

  //Log the data that was read.
  Logger.log(JSON.stringify(data));

  return dataMap;
}

function setValidation(sheet, range, rule){
  var cell = sheet.getRange(range);
  cell.setDataValidation(rule);
}

function getUserListRule(){
  var userNameList = Array.from(userList.keys());
  return SpreadsheetApp.newDataValidation()
      .requireValueInList(userNameList)
      .setAllowInvalid(false)
      .setHelpText('Please set valide user name.')
      .build();
}

function getUserRoleListRule(){
  return SpreadsheetApp.newDataValidation()
      .requireValueInList(Object.keys(calendarRoleMap))
      .setAllowInvalid(false)
      .setHelpText('Please specify valide user type.')
      .build();
}
