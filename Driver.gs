var BRIFolderID = "1wuxRS8lOZ3E4qp-AJs_oqIm3wHo_obdT";
var AITFolderID = "1mOpE1i2F_sNtCb15LPRFzrxbu6rYh1rT";

function start(){
  getFolderTree(AITFolderID, true);
};

// Get Folder Tree
function getFolderTree(folderId, listAll) {
  try {
    // Get folder by id
    var parentFolder = DriveApp.getFolderById(folderId);
    var dataSheetIds = getSubFolders(parentFolder.getName(), parentFolder, dataSheetIds);
    if(dataSheetIds.length > 0){
      createStatisticsSheet(parentFolder, dataSheetIds);
    }
  } catch (e) {
    Logger.log(e.toString(), e);
  }
}

function getSubFolders(parentName, parent) {

  var dataSheetIds = [];
  // List folders inside the folder
  var childFolders = parent.getFolders();
  while (childFolders.hasNext()) {
    var childFolder = childFolders.next();
    Logger.log("Folder Name: " + childFolder.getName());
    // Recursive call of the subfolder
    var subDataSheetIds = getSubFolders(parentName + "/" + childFolder.getName(), childFolder, dataSheetIds);
    if(subDataSheetIds.length > 0){
      dataSheetIds = dataSheetIds.concat(subDataSheetIds);
    }
  }

  // List files inside the folder
  var childFiles = parent.getFiles();
  while (childFiles.hasNext()) {
    var childFile = childFiles.next();
    var departmentProject = parentName + "/" + childFile.getName();
    Logger.log(departmentProject);
    if(childFile.getMimeType().indexOf("spreadsheet") > -1){
      if(isDataSheet(childFile)){
        Logger.log(childFile.getId());
        dataSheetIds.push([childFile.getId(), departmentProject]);
        childFile.setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW)
        openEventSheet(childFile, parentName);
      }
    }
  }
  if(dataSheetIds.length > 0){
    createStatisticsSheet(parent, dataSheetIds);
  }
  return dataSheetIds;
}

function getFile(fileName, folder){
  var childFiles = folder.getFiles();
  while (childFiles.hasNext()) {
    var childFile = childFiles.next();
    if(childFile.getName() === fileName){
      Logger.log(`File ${fileName} exist`);
      return childFile;
    }
  }
}
