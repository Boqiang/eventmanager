var calendarRoleMap = {
  "admin":"owner",
  "counselor":"reader",
  "teacher":"reader",
  "student":"reader",
};

function createCalendar(name) {
  // Creates a new calendar named "Travel Plans".
  var calendars = CalendarApp.getCalendarsByName(name);
  var calendar;
  if(calendars.length == 0){
    calendar = CalendarApp.createCalendar(name);
    Logger.log('Created the calendar "%s", with the ID "%s".',
        calendar.getName(), calendar.getId());
  }else{
    Logger.log('Calendar "%s" exists', name);
    calendar = calendars[0];
  }
  return calendar;
}

function createEvent(calendar, title, day, startTimeValue, endTimeValue, description, location){

    // set start and end time
    var startTime = new Date();
    startTime.setFullYear(day.getFullYear());
    startTime.setMonth(day.getMonth());
    startTime.setDate(day.getDate());
    startTime.setHours(startTimeValue.getHours());
    startTime.setMinutes(startTimeValue.getMinutes());

    var endTime = new Date();
    endTime.setFullYear(day.getFullYear());
    endTime.setMonth(day.getMonth());
    endTime.setDate(day.getDate());
    endTime.setHours(endTimeValue.getHours());
    endTime.setMinutes(endTimeValue.getMinutes());

    Logger.log(startTime);
    Logger.log(endTime);

    // check existing events
    var allEvents = calendar.getEvents(startTime, endTime);//Returns an array of events
    if (allEvents.length > 0) {
      Logger.log('Events "%s-%s" exists', startTime, endTime);
      return;//Don't try to add a new calendar event - quit here
    };

    // create event
    calendar.createEvent(title,startTime,endTime, {location:location, description:description});
}

function shareCalendar( calendar, user, role ) {
  // skip owner
  if(calendar.isOwnedByMe()){
    return;
  }

  var calId = calendar.getId();
  role = role || "reader";
  var acl = null;
  // Check whether there is already a rule for this user
  try {
    var acl = Calendar.Acl.get(calId, "user:"+user);
  }
  catch (e) {
    Logger.log(e);
    // no existing acl record for this user - as expected. Carry on.
  }

  if (!acl) {
    // No existing rule - insert one.
    acl = {
      "scope": {
        "type": "user",
        "value": user
      },
      "role": role
    };
    var newRule = Calendar.Acl.insert(acl, calId);
  }
  else {
    // There was a rule for this user - update it.
    acl.role = role;
    newRule = Calendar.Acl.update(acl, calId, acl.id)
  }
  return newRule;
}

function getCalendarRole(userType){
  if(userType){
    var role = calendarRoleMap[userType.toLowerCase()];
    return role;
  }
}