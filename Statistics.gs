
function testStatistics() {
  var sheetIds = ["1bLfjAK5s7sIk74zE-sHAJ1yN4ROn3H87i3n62jc7rlc", "1bLfjAK5s7sIk74zE-sHAJ1yN4ROn3H87i3n62jc7rlc", "1bLfjAK5s7sIk74zE-sHAJ1yN4ROn3H87i3n62jc7rlc"];
  Logger.log(getAllImportRange(sheetIds,"user!A2:A"));
  Logger.log(getUnique(getAllImportRange(sheetIds,"user!A2:A")));
}

function getAllImportRange(sheetIds, sheetNameAndRange){
  var importString = "";
  var separater = "";
  for(var i = 0; i < sheetIds.length; i ++){
    importString += `${separater}importrange("${sheetIds[i][0]}","${sheetNameAndRange}")`
    separater = ";";
  }
  return `{${importString}}`;
}

function getUnique(data){
  return `unique(${data})`;
}

function getQuery(data, query){
  return `query(${data}, "${query}")`;
}

function getVlookup(toBeSearch, data, result){
  return `vlookup(${toBeSearch}, ${data}, ${result})`;
}

function setValuesForImport(sheet, startColumn, lookupSheetId, departmentProject){
  var values = [[departmentProject]];
  for(var i = 2; i < 200; i ++){
    values.push([`=IFERROR(VLOOKUP(A${i}, importrange("${lookupSheetId}","'user'!A2:C"), 3))`]);
  }
  sheet.getRange(1,startColumn,199,1).setValues(values);
}

function setFinalSum(sheet){
  var values = [["Sum"]];
  for(var i = 2; i < 200; i ++){
    values.push([`=sum(C${i}:XX${i})`]);
  }
  sheet.getRange(1,2,199,1).setValues(values);
}

function createStatisticsSheet(folder, dataSheetIds){
  var f = getFile("statistics", folder);
  if(!f){
    var sheetFile = SpreadsheetApp.create("statistics");
    f = DriveApp.getFileById(sheetFile.getId());
    f.moveTo(folder);
//    f = folder.addFile(file);
  }
  var sheetFile = SpreadsheetApp.openById(f.getId());
  var sheet = sheetFile.getSheets()[0];
  sheet.getRange("A2:A2").setValue(`=${getUnique(getAllImportRange(dataSheetIds,"'user'!A2:A"))}`);

  // set value for each department project
  for(var i = 0; i < dataSheetIds.length; i ++){
    Logger.log(`data sheets & projects: ${dataSheetIds}`);
    sheet.getRange(2,3,1,1).setValue(`=${getVlookup('A2',getQuery(getAllImportRange(dataSheetIds,"'user'!A2:C"),"select Col1, sum(Col3) where Col1<>'' group by Col1"),2)}`);
//    setValuesForImport(sheet, 3 + i, dataSheetIds[i][0], dataSheetIds[i][1]);
  }

  // set name
  sheet.getRange("A1:A1").setValue("Name");

  // set final sum
  setFinalSum(sheet);

  // set layout
  sheet.autoResizeRows(1,1);
  sheet.autoResizeColumns(3,dataSheetIds.length);
  sheet.setFrozenColumns(2);
  sheet.setFrozenRows(1);
}