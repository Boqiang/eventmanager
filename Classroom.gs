function listCourses() {
  var optionalArgs = {
    pageSize: 10
  };
  var response = Classroom.Courses.list(optionalArgs);
  var courses = response.courses;
  if (courses && courses.length > 0) {
    for (i = 0; i < courses.length; i++) {
      var course = courses[i];
      Logger.log('%s (%s)', course.name, course.id);
    }
  } else {
    Logger.log('No courses found.');
  }
}


function createCourse(projectName, description) {
  var optionalArgs = {
    pageSize: 10
  };
  // var response = Classroom.Courses.create();

  var course = Classroom.Courses.create({
    "ownerId": Session.getActiveUser().getEmail(),
    "name": projectName,
    "section": projectName,
    "descriptionHeading": projectName,
    "description": description,
      "courseMaterialSets" : [{
              "title" : projectName,
              "materials" : [{
                      // "driveFile" : { 
                      //         "id" : getData[i][5],
                      //         "title" : 'Course Outline' ,
                      //         "alternateLink": getData[i][4], 
                      //         "thumbnailUrl" : 'https://drive.google.com/uc?export=download&id=-Image ID-',

                      // },
                  }
              ]
          }
      ]
  })
  Logger.log(course);

  return course;
}

function addTeacher(course, email){
    Classroom.Courses.Teachers.create(
      {
        "userId": email
      }, course.id);
}